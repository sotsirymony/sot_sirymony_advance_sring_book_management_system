package book_management.example.demo.Repository;

import book_management.example.demo.Entity.Book;
import book_management.example.demo.Entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRespository extends JpaRepository<Category,Integer> {
//    @Query("SELECT p FROM Category  p WHERE p.title LIKE %?1%"
//            + " OR p.id LIKE %?1%")
//    public List<Category> search(String keyword);
}
