package book_management.example.demo.Controller;

import book_management.example.demo.Entity.Book;
import book_management.example.demo.Entity.Category;
import book_management.example.demo.Repository.BookRepository;
import book_management.example.demo.Repository.CategoryRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class BookController {
    @Autowired
    private BookRepository repository;
    @GetMapping("/books")
    public List<Book> getAllBooks() {
        return repository.findAll();
    }
    @PostMapping("/books")
    Book createOrSaveBook(@RequestBody Book book) {
        return repository.save(book);
    }
    @GetMapping("/books/{id}")
    Optional<Book> getBookById(@PathVariable int id) {
        return repository.findById(id);
    }
    @PutMapping("/books/{id}")
    Book updateBook(@RequestBody Book newBook, @PathVariable int id) {
        return repository.findById(id).map(book -> {
            book.setAuthor(newBook.getAuthor());
            book.setDescription(newBook.getDescription());
            book.setThumnail(newBook.getThumnail());
            book.setTitle(newBook.getTitle());
            return repository.save(book);
        }).orElseGet(() -> {
            newBook.setId(id);
            return repository.save(newBook);
        });
    }
    @DeleteMapping("/books/{id}")
    public String deleteBook(@PathVariable int id)
    {
        repository.deleteById(id);
        return "Book has been deleted";
    }
}
