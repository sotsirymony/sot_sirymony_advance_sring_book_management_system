package book_management.example.demo.Controller;
import book_management.example.demo.Entity.Category;
import book_management.example.demo.Repository.CategoryRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class CategoryController {
    @Autowired
    private CategoryRespository repository;
    @GetMapping("/categorys")
    public List<Category> getAllCategorys() {
        return repository.findAll();
    }
    @PostMapping("/categorys")
    Category createOrSaveCategory(@RequestBody Category category) {
        return repository.save(category);
    }
    @GetMapping("/categorys/{id}")
    Optional<Category> getCategoryById(@PathVariable int id) {
        return repository.findById(id);
    }
    @PutMapping("/categorys/{id}")
    Category updateEmployee(@RequestBody Category newCategory, @PathVariable int id) {
        return repository.findById(id).map(category -> {
            category.setTitle(newCategory.getTitle());
            return repository.save(category);
        }).orElseGet(() -> {
            newCategory.setId(id);
            return repository.save(newCategory);
        });
    }
    @DeleteMapping("/categorys/{id}")
    public String deleteCategory(@PathVariable int id)
    {
        repository.deleteById(id);
        return "Category has been deleted";
    }

}
